package uns.ac.rs.ftn.textplagplugin;

import static java.util.UUID.randomUUID;
import static uns.ac.rs.ftn.coreinterfaces.port.common.CommonMetadataFields.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import uns.ac.rs.ftn.coreinterfaces.port.common.Dataset;
import uns.ac.rs.ftn.coreinterfaces.port.common.DocumentMetadata;
import uns.ac.rs.ftn.coreinterfaces.port.common.StringRange;
import uns.ac.rs.ftn.coreinterfaces.port.detector.DetectionResult;
import uns.ac.rs.ftn.coreinterfaces.port.detector.DetectorPlugin;
import uns.ac.rs.ftn.coreinterfaces.port.detector.Match;

public class Plugin implements DetectorPlugin {

  @Override
  public DetectionResult detect(Dataset dataset) {
    String candidatePath = getFullTextPath(dataset.getCandidateDocumentMetadata());

    List<Match> matches = dataset.getReferenceDocumentsMetadataData().stream().filter(referenceMetadata -> {
      String type = (String) referenceMetadata.getMetadata().get(MIMETYPE);
      return "application/pdf".equals(type) || "text/plain".equals(type);
    })
      .map(referenceMetadata -> checkDocument(referenceMetadata, candidatePath))
      .filter(Objects::nonNull)
      .collect(Collectors.toList());

    return new DetectionResult(matches);
  }

  private Match checkDocument(DocumentMetadata referenceMetadata, String candidatePath) {
    String referencePath = getFullTextPath(referenceMetadata);

    String tempLogFileName = randomUUID().toString();

    try {
      Process process =
          new ProcessBuilder("Python39\\Scripts\\text-matcher.exe", "-l", tempLogFileName, referencePath, candidatePath)
            .start();
      int exitCode = process.waitFor();
      if (exitCode != 0) {
        return null;
      }
      List<String> lines;
      try (InputStream is = new FileInputStream(tempLogFileName)) {
        lines = IOUtils.readLines(is, Charset.defaultCharset());
      }
      FileUtils.delete(new File(tempLogFileName));

      if (lines.size() < 2) {
        return null;
      }
      String result = lines.get(1);
      String[] columns = result.split("\",\"");
      for (int i = 0; i < columns.length; i++) {
        columns[i] = columns[i].trim().replace("\"", "");
      }
      if (columns.length < 10) {
        return null;
      }
      int candidateLength = Integer.parseInt(columns[7]);
      List<StringRange> referenceRanges = StringRange.parse(columns[8], referencePath);
      List<StringRange> candidateRanges = StringRange.parse(columns[9], candidatePath);

      int plagiarismLength =
          candidateRanges.stream().map(range -> range.getEnd() - range.getStart()).reduce(0, Integer::sum);

      return new Match(referenceMetadata, plagiarismLength * 1D / candidateLength, candidateRanges, referenceRanges);
    } catch (Exception e) {
      e.printStackTrace();
      try {
        FileUtils.delete(new File(tempLogFileName));
      } catch (Exception ignore) {
      }
      return null;
    }
  }

  private String getFullTextPath(DocumentMetadata documentMetadata) {
    if ("text/plain".equals(documentMetadata.get(MIMETYPE))) {
      return documentMetadata.getFilePath();
    }
    if ("txt".equals(documentMetadata.get(EXTENSION))) {
      return documentMetadata.getFilePath();
    }
    return documentMetadata.get(FULL_TEXT_PATH);
  }
}
