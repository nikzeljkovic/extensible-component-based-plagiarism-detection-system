package uns.ac.rs.ftn.textplagplugin;

import static uns.ac.rs.ftn.coreinterfaces.port.common.CommonMetadataFields.FULL_TEXT_PATH;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import uns.ac.rs.ftn.coreinterfaces.port.common.Dataset;
import uns.ac.rs.ftn.coreinterfaces.port.common.DocumentMetadata;
import uns.ac.rs.ftn.coreinterfaces.port.detector.DetectionResult;

public class Main {

  public static void main(String[] args) {
    DetectionResult detectionResult = new Plugin().detect(new Dataset() {

      @Override
      public DocumentMetadata getCandidateDocumentMetadata() {
        return getDocumentMetadata("D:\\Dev\\plag-node\\sandbox\\test.txt");
      }

      private DocumentMetadata getDocumentMetadata(String path) {
        Map<String, Object> metadata = new HashMap<>();
        metadata.put(FULL_TEXT_PATH, path);
        return new DocumentMetadata("asd", "asd", metadata);
      }

      @Override
      public List<DocumentMetadata> getReferenceDocumentsMetadataData() {
        return Arrays.asList(getDocumentMetadata("D:\\Dev\\plag-node\\sandbox\\test2.txt"),
            getDocumentMetadata("D:\\Dev\\plag-node\\sandbox\\test3.txt"));
      }
    });

    System.out.println(detectionResult);
  }
}
