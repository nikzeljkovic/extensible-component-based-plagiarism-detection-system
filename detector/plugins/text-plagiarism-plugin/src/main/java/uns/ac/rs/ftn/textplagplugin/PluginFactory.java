package uns.ac.rs.ftn.textplagplugin;

import static uns.ac.rs.ftn.coreinterfaces.port.common.CommonMetadataFields.FULL_TEXT_PATH;

import java.util.Collections;

import uns.ac.rs.ftn.coreinterfaces.port.common.PluginMetadata;
import uns.ac.rs.ftn.coreinterfaces.port.detector.DetectorPlugin;
import uns.ac.rs.ftn.coreinterfaces.port.detector.DetectorPluginFactory;

public class PluginFactory implements DetectorPluginFactory {

  @Override
  public PluginMetadata getPluginMetadata() {
    return new PluginMetadata("text-detection-plugin", "text-detection-plugin", "Text plagiarism detection plugin",
        Collections.singletonList(FULL_TEXT_PATH), Collections.emptyList());
  }

  @Override
  public DetectorPlugin createInstance() {
    return new Plugin();
  }
}
