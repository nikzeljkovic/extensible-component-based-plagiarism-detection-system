package uns.ac.rs.ftn.pdftextextractionplugin;

import static uns.ac.rs.ftn.coreinterfaces.port.common.CommonMetadataFields.MIMETYPE;

import java.util.HashMap;
import java.util.Map;

import uns.ac.rs.ftn.coreinterfaces.port.common.DocumentMetadata;

public class Main {

  public static void main(String[] args) {
    Map<String, Object> metadata = new HashMap<>();
    metadata.put(MIMETYPE, "application/pdf");
    new Plugin().process(new DocumentMetadata("asd",
        "D:\\Dev\\plag-node\\protocol\\documents\\2c69f1f1-f593-46c1-b7b2-219060b2c7af.pdf", metadata), a -> {
          System.out.println(a);
        });
  }
}
