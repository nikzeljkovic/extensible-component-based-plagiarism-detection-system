package uns.ac.rs.ftn.pdftextextractionplugin;

import static java.util.UUID.randomUUID;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.function.Consumer;

import org.apache.commons.io.FileUtils;
import org.apache.pdfbox.io.RandomAccessFile;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.tika.Tika;

import uns.ac.rs.ftn.coreinterfaces.port.common.CommonMetadataFields;
import uns.ac.rs.ftn.coreinterfaces.port.common.DocumentMetadata;
import uns.ac.rs.ftn.coreinterfaces.port.common.PluginMetadata;
import uns.ac.rs.ftn.coreinterfaces.port.processing.ProcessingPlugin;

public class Plugin implements ProcessingPlugin {

  private static final Path FULL_TEXT_PATH = Paths.get("fulltext");

  @Override
  public PluginMetadata getPluginMetadata() {
    return new PluginMetadata("pdf-fulltext", "Text extraction plugin for pdf files.", Collections.emptyList());
  }

  @Override
  public String getResultFieldName() {
    return CommonMetadataFields.FULL_TEXT_PATH;
  }

  @Override
  public void process(DocumentMetadata documentMetadata, Consumer<Object> storeValueForDocument) {
    String mimeType = documentMetadata.get(CommonMetadataFields.MIMETYPE);
    String extension = documentMetadata.get(CommonMetadataFields.EXTENSION);
    if (isNotPdf(mimeType) && isNotPdf(extension)) {
      if ("text/plain".equals(mimeType)) {
        storeValueForDocument.accept(documentMetadata.getFilePath());
      }
      return;
    }
    String fullText = extractFullText(new File(documentMetadata.getFilePath()));
    String fullTextFileName = randomUUID().toString();
    try {
      File file = FULL_TEXT_PATH.resolve(fullTextFileName).toFile();
      FileUtils.writeStringToFile(file, fullText, Charset.defaultCharset());
      storeValueForDocument.accept(file.getAbsolutePath());
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private boolean isNotPdf(String mimeTypeOrExtension) {
    return !"application/pdf".equalsIgnoreCase(mimeTypeOrExtension) && !"pdf".equalsIgnoreCase(mimeTypeOrExtension);
  }

  private String extractFullText(File file) {
    try {
      return extractFullTextPDFParser(file);
    } catch (Exception e) {
      return extractFullTextTika(file);
    }
  }

  private String extractFullTextTika(File file) {
    try {
      Tika tika = new Tika();
      tika.setMaxStringLength(-1);
      return tika.parseToString(file);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  private String extractFullTextPDFParser(File file) throws IOException {
    PDFParser parser = new PDFParser(new RandomAccessFile(file, "r"));
    parser.parse();
    PDFTextStripper textStripper = new PDFTextStripper();
    String text = textStripper.getText(parser.getPDDocument());
    parser.getPDDocument().close();

    return text;
  }
}
