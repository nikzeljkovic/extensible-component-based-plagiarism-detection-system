package uns.ac.rs.ftn.md5plugin;

import java.util.Collections;
import java.util.function.Consumer;

import uns.ac.rs.ftn.coreinterfaces.port.common.DocumentMetadata;
import uns.ac.rs.ftn.coreinterfaces.port.common.PluginMetadata;
import uns.ac.rs.ftn.coreinterfaces.port.processing.ProcessingPlugin;

public class P2 implements ProcessingPlugin {

  @Override
  public PluginMetadata getPluginMetadata() {
    return new PluginMetadata("P2", "P2 desc", Collections.singletonList("P3"));
  }

  @Override
  public String getResultFieldName() {
    return "p2";
  }

  @Override
  public void process(DocumentMetadata documentMetadata, Consumer<Object> storeValueForDocument) {

  }
}
