package uns.ac.rs.ftn.md5plugin;

import java.util.Arrays;
import java.util.function.Consumer;

import uns.ac.rs.ftn.coreinterfaces.port.common.DocumentMetadata;
import uns.ac.rs.ftn.coreinterfaces.port.common.PluginMetadata;
import uns.ac.rs.ftn.coreinterfaces.port.processing.ProcessingPlugin;

public class P1 implements ProcessingPlugin {

  @Override
  public PluginMetadata getPluginMetadata() {
    return new PluginMetadata("P1", "P1 desc", Arrays.asList("P2", "P3"));
  }

  @Override
  public String getResultFieldName() {
    return "p1";
  }

  @Override
  public void process(DocumentMetadata documentMetadata, Consumer<Object> storeValueForDocument) {

  }
}
