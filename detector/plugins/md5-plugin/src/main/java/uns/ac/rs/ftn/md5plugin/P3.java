package uns.ac.rs.ftn.md5plugin;

import java.util.Collections;
import java.util.function.Consumer;

import uns.ac.rs.ftn.coreinterfaces.port.common.DocumentMetadata;
import uns.ac.rs.ftn.coreinterfaces.port.common.PluginMetadata;
import uns.ac.rs.ftn.coreinterfaces.port.processing.ProcessingPlugin;

public class P3 implements ProcessingPlugin {

  @Override
  public PluginMetadata getPluginMetadata() {
    return new PluginMetadata("P3", "P3 desc", Collections.singletonList("P0"));
  }

  @Override
  public String getResultFieldName() {
    return "p3";
  }

  @Override
  public void process(DocumentMetadata documentMetadata, Consumer<Object> storeValueForDocument) {

  }
}
