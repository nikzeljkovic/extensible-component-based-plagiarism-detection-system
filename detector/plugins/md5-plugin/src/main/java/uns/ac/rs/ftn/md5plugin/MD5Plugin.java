package uns.ac.rs.ftn.md5plugin;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.function.Consumer;

import org.apache.commons.codec.digest.DigestUtils;

import uns.ac.rs.ftn.coreinterfaces.port.common.DocumentMetadata;
import uns.ac.rs.ftn.coreinterfaces.port.common.PluginMetadata;
import uns.ac.rs.ftn.coreinterfaces.port.processing.ProcessingPlugin;

public class MD5Plugin implements ProcessingPlugin {

  @Override
  public PluginMetadata getPluginMetadata() {
    return new PluginMetadata("md5-plugin", "Create an 'md5' entry in the document's metadata.",
        Collections.emptyList());
  }

  @Override
  public String getResultFieldName() {
    return "md5";
  }

  @Override
  public void process(DocumentMetadata documentMetadata, Consumer<Object> storeValueForDocument) {
    try (InputStream is = Files.newInputStream(Paths.get(documentMetadata.getFilePath()))) {
      String md5 = DigestUtils.md5Hex(is);
      storeValueForDocument.accept(md5);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
