package uns.ac.rs.ftn.pythonplugin;

import static uns.ac.rs.ftn.coreinterfaces.port.common.CommonMetadataFields.MIMETYPE;

import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;

import uns.ac.rs.ftn.coreinterfaces.port.common.Dataset;
import uns.ac.rs.ftn.coreinterfaces.port.common.DocumentMetadata;
import uns.ac.rs.ftn.coreinterfaces.port.detector.DetectionResult;
import uns.ac.rs.ftn.coreinterfaces.port.detector.DetectorPlugin;
import uns.ac.rs.ftn.coreinterfaces.port.detector.Match;

public class Plugin implements DetectorPlugin {

  public static final double DETECTION_THRESHOLD = 0.5;

  @Override
  public DetectionResult detect(Dataset dataset) {
    String candidatePath = getFullTextPath(dataset.getCandidateDocumentMetadata());
    List<Match> matches = dataset.getReferenceDocumentsMetadataData().stream().filter(referenceMetadata -> {
      String type = (String) referenceMetadata.getMetadata().get(MIMETYPE);
      return "text/x-python".equals(type);
    })
      .map(referenceDocumentMetadata -> new Match(referenceDocumentMetadata,
          calculateMatchPercentage(getFullTextPath(referenceDocumentMetadata), candidatePath)))
      .filter(match -> match.getScore() != null)
      .filter(match -> match.getScore() > DETECTION_THRESHOLD)
      .collect(Collectors.toList());

    return new DetectionResult(matches);
  }

  private Double calculateMatchPercentage(String referencePath, String candidatePath) {
    try {
      Process process =
          new ProcessBuilder("Python39\\Scripts\\pycode_similar.exe", referencePath, candidatePath).start();
      int exitCode = process.waitFor();
      if (exitCode != 0) {
        return null;
      }
      InputStream inputStream = process.getInputStream();
      if (inputStream == null) {
        return null;
      }
      List<String> lines = IOUtils.readLines(inputStream, Charset.defaultCharset());
      if (lines.size() <= 2) {
        return null;
      }
      String resultLine = lines.get(2);
      if (!resultLine.contains(" % ")) {
        return null;
      }
      String result = resultLine.split(" % ")[0].trim();
      if (result.length() == 0) {
        return null;
      }
      return Double.parseDouble(result) / 100D;
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  private String getFullTextPath(DocumentMetadata documentMetadata) {
    return documentMetadata.getFilePath();
  }

}
