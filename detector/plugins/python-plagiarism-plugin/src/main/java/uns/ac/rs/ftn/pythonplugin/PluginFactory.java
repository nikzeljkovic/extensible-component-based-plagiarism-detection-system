package uns.ac.rs.ftn.pythonplugin;

import java.util.Collections;

import uns.ac.rs.ftn.coreinterfaces.port.common.PluginMetadata;
import uns.ac.rs.ftn.coreinterfaces.port.detector.DetectorPlugin;
import uns.ac.rs.ftn.coreinterfaces.port.detector.DetectorPluginFactory;

public class PluginFactory implements DetectorPluginFactory {

  @Override
  public PluginMetadata getPluginMetadata() {
    return new PluginMetadata("python-plugin", "Plugin for detecting source code plagiarism of python sources.",
        Collections.emptyList());
  }

  @Override
  public DetectorPlugin createInstance() {
    return new Plugin();
  }
}
