package uns.ac.rs.ftn.defaultdocumentacessplugin;

import java.io.IOException;
import java.util.Collections;

import com.google.gson.Gson;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import uns.ac.rs.ftn.coreinterfaces.port.common.DataAccessPlugin;
import uns.ac.rs.ftn.coreinterfaces.port.common.Dataset;
import uns.ac.rs.ftn.coreinterfaces.port.common.DocumentMetadata;
import uns.ac.rs.ftn.coreinterfaces.port.common.PluginMetadata;

public class Plugin implements DataAccessPlugin {

  @Override
  public PluginMetadata getPluginMetadata() {
    return new PluginMetadata("default-dataset", "Plugin for accessing data from the document server.",
        Collections.emptyList());
  }

  @Override
  public Dataset loadDataset(DocumentMetadata candidateDocument) {
    try {
      OkHttpClient client = new OkHttpClient();
      Request request =
          new Request.Builder().url("http://localhost:55550/api/v1/documents?partOfReferenceSet=true").get().build();
      Response response = client.newCall(request).execute();
      ResponseBody body = response.body();
      if (body == null) {
        return new Dataset(candidateDocument, Collections.emptyList());
      }

      Documents documents = new Gson().fromJson(body.charStream(), Documents.class);
      return new Dataset(candidateDocument, documents);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return new Dataset(candidateDocument, Collections.emptyList());
  }
}
