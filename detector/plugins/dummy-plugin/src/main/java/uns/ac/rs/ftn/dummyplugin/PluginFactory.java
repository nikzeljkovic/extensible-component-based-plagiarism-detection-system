package uns.ac.rs.ftn.dummyplugin;

import java.util.Collections;

import uns.ac.rs.ftn.coreinterfaces.port.common.PluginMetadata;
import uns.ac.rs.ftn.coreinterfaces.port.detector.DetectorPlugin;
import uns.ac.rs.ftn.coreinterfaces.port.detector.DetectorPluginFactory;

public class PluginFactory implements DetectorPluginFactory {

  @Override
  public DetectorPlugin createInstance() {
    return new Plugin();
  }

  @Override
  public PluginMetadata getPluginMetadata() {
    return new PluginMetadata("dummy-plugin", "This is a dummy detector plugin for demo purposes.",
        Collections.emptyList());
  }
}
