package uns.ac.rs.ftn.coreinterfaces.port.detector;

public enum DetectionTaskStatus {
  PENDING,
  RUNNING,
  COMPLETED
}
