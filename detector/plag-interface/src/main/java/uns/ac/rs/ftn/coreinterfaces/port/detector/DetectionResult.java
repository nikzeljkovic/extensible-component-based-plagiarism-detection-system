package uns.ac.rs.ftn.coreinterfaces.port.detector;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DetectionResult {

  private final List<Match> matches;
}
