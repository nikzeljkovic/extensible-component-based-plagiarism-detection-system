package uns.ac.rs.ftn.coreinterfaces.port.common;

import java.util.Collections;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PluginMetadata {

  private String name;

  private String humanReadableName;

  private String description;

  private List<String> requiredDocumentMetadata;

  private List<String> dependencies;

  public PluginMetadata(String name, String description, List<String> dependencies) {
    this(name, name, description, Collections.emptyList(), dependencies);
  }
}
