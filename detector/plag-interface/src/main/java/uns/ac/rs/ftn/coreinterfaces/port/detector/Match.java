package uns.ac.rs.ftn.coreinterfaces.port.detector;

import java.util.Collections;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uns.ac.rs.ftn.coreinterfaces.port.common.DocumentMetadata;
import uns.ac.rs.ftn.coreinterfaces.port.common.StringRange;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Match {

  private DocumentMetadata documentMetadata;

  private Double score;

  private List<StringRange> rangesInCandidateDocument;

  private List<StringRange> rangesInReferenceDocument;

  public Match(DocumentMetadata documentMetadata, Double score) {
    this(documentMetadata, score, Collections.emptyList(), Collections.emptyList());
  }
}
