package uns.ac.rs.ftn.coreinterfaces.port.detector;

import uns.ac.rs.ftn.coreinterfaces.port.common.Dataset;

public interface DetectorPlugin {

  DetectionResult detect(Dataset dataset);
}
