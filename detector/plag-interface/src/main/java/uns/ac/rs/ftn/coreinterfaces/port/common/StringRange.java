package uns.ac.rs.ftn.coreinterfaces.port.common;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StringRange {

  private int start;

  private int end;

  private String text;

  public static List<StringRange> parse(String string, String sourcePath) {
    string = string.replace("[", "");
    string = string.replace("]", "");

    String[] ranges = string.split("\\), ");

    return Arrays.stream(ranges).map(range -> {
      range = range.replace("(", "");
      range = range.replace(")", "");
      String[] startEnd = range.split(", ");
      int start = Integer.parseInt(startEnd[0]);
      int end = Integer.parseInt(startEnd[1]);
      StringRange stringRange = new StringRange(start, end, null);
      stringRange.setText(stringRange.apply(new File(sourcePath)));
      return stringRange;
    }).collect(Collectors.toList());
  }

  public String apply(File file) {
    try {
      String fileContent = FileUtils.readFileToString(file, Charset.defaultCharset());
      return fileContent.substring(start, end);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }
}
