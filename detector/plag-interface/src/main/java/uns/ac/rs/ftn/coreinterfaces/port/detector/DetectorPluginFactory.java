package uns.ac.rs.ftn.coreinterfaces.port.detector;

import uns.ac.rs.ftn.coreinterfaces.port.common.Plugin;

public interface DetectorPluginFactory extends Plugin {

  DetectorPlugin createInstance();
}
