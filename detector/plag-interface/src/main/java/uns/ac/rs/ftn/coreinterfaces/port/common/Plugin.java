package uns.ac.rs.ftn.coreinterfaces.port.common;

public interface Plugin {

  PluginMetadata getPluginMetadata();
}
