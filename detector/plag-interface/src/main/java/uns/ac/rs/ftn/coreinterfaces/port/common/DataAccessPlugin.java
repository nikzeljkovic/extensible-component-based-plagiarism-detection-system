package uns.ac.rs.ftn.coreinterfaces.port.common;

public interface DataAccessPlugin extends Plugin {

  Dataset loadDataset(DocumentMetadata candidateDocument);
}
