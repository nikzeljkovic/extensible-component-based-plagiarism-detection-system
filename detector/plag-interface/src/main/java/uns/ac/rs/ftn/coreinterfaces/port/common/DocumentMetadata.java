package uns.ac.rs.ftn.coreinterfaces.port.common;

import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DocumentMetadata {

  private String fileName;

  private String filePath;

  private Boolean processingDone;

  private Map<String, Object> metadata;

  public DocumentMetadata(String fileName, String filePath, Map<String, Object> metadata) {
    this(fileName, filePath, false, metadata);
  }

  public <T> T get(String metadataFieldName) {
    if (metadata == null) {
      return null;
    }
    try {
      return (T) metadata.get(metadataFieldName);
    } catch (ClassCastException e) {
      return null;
    }
  }
}
