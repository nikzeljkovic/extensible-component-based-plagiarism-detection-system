package uns.ac.rs.ftn.coreinterfaces.port.common;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Dataset {

  private DocumentMetadata candidate;

  private List<DocumentMetadata> references;

  public DocumentMetadata getCandidateDocumentMetadata() {
    return candidate;
  }

  public List<DocumentMetadata> getReferenceDocumentsMetadataData() {
    return references;
  }
}
