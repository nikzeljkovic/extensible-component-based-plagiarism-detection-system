package uns.ac.rs.ftn.coreinterfaces.port.common;

public final class CommonMetadataFields {

  public static final String FULL_TEXT_PATH = "fullTextPath";

  public static final String MIMETYPE = "mimetype";

  public static final String EXTENSION = "extension";

  public static final String MD5 = "md5";

  public static final String FILE_SIZE_BYTES = "fileSizeBytes";

  public static final String FULL_TEXT_SIZE_BYTES = "fullTextSizeBytes";

  private CommonMetadataFields() {
    super();
  }
}
