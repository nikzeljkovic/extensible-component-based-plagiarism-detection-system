package uns.ac.rs.ftn.coreinterfaces.port.processing;

import java.util.function.Consumer;

import uns.ac.rs.ftn.coreinterfaces.port.common.DocumentMetadata;
import uns.ac.rs.ftn.coreinterfaces.port.common.Plugin;

public interface ProcessingPlugin extends Plugin {

  String getResultFieldName();

  void process(DocumentMetadata documentMetadata, Consumer<Object> storeValueForDocument);
}
