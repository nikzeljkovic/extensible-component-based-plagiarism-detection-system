package rs.ac.uns.ftn.authservice.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class Role {

  private String name;
}
