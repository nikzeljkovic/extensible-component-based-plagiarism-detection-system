package uns.ac.rs.ftn.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
@ComponentScan(basePackages = { "uns.ac.rs.ftn.core", "rs.ac.uns.ftn.plag.restclient" })
public class DetectorServerApp {

  public static void main(String[] args) {
    SpringApplication.run(DetectorServerApp.class, args);
  }
}
