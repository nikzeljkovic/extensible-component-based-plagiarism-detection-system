package uns.ac.rs.ftn.core.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import uns.ac.rs.ftn.core.entity.DetectionTask;
import uns.ac.rs.ftn.coreinterfaces.port.detector.DetectionTaskStatus;

public interface DetectionTaskRepository extends MongoRepository<DetectionTask, String> {

  List<DetectionTask> findAllByStatus(DetectionTaskStatus status);

  List<DetectionTask> findAllByCreatorId(String currentUserId);

  Optional<DetectionTask> findByIdAndCreatorId(String id, String currentUserId);
}
