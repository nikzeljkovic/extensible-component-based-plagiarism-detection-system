package uns.ac.rs.ftn.core.service;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import uns.ac.rs.ftn.coreinterfaces.port.common.PluginMetadata;
import uns.ac.rs.ftn.coreinterfaces.port.detector.DetectorPlugin;
import uns.ac.rs.ftn.coreinterfaces.port.detector.DetectorPluginFactory;

@Service
public class DetectorService extends AbstractPluginService<DetectorPluginFactory> {

  protected DetectorService() {
    super(DetectorPluginFactory.class);
  }

  public List<PluginMetadata> getPluginsMetadata() {
    reloadPlugins();
    return plugins.stream().map(DetectorPluginFactory::getPluginMetadata).collect(Collectors.toList());
  }

  public DetectorPlugin getDetectorByName(String detectorName) {
    return plugins.stream()
      .filter(p -> StringUtils.equals(detectorName, p.getPluginMetadata().getName()))
      .findFirst()
      .map(DetectorPluginFactory::createInstance)
      .orElse(null);
  }
}
