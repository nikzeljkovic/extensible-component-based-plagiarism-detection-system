package uns.ac.rs.ftn.core.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import lombok.RequiredArgsConstructor;
import uns.ac.rs.ftn.core.entity.DetectionTask;
import uns.ac.rs.ftn.core.service.DetectionTaskService;

@RestController
@RequestMapping("/api/v1/detection-tasks")
@RequiredArgsConstructor
public class DetectionTasksController {

  private final DetectionTaskService detectionTaskService;

  @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<DetectionTask> post(@RequestBody DetectionTask detectionTask) {
    return ResponseEntity.ok(detectionTaskService.createDetectionTask(detectionTask));
  }

  @GetMapping(produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<List<DetectionTask>> get() {
    return ResponseEntity.ok(detectionTaskService.findAll());
  }
}
