package uns.ac.rs.ftn.core.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import uns.ac.rs.ftn.core.entity.DetectionTask;
import uns.ac.rs.ftn.core.service.DetectionTaskService;

@RestController
@RequestMapping("/api/v1/detection-task/{id}")
@RequiredArgsConstructor
public class DetectionTaskController {

  private final DetectionTaskService detectionTaskService;

  @GetMapping(produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<DetectionTask> findById(@PathVariable String id) {
    return ResponseEntity.ok(detectionTaskService.findById(id));
  }
}
