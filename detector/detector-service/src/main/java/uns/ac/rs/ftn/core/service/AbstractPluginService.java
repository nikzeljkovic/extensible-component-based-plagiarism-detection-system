package uns.ac.rs.ftn.core.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;

import uns.ac.rs.ftn.coreinterfaces.port.common.Plugin;
import uns.ac.rs.ftn.pluginutils.PluginLoader;

public abstract class AbstractPluginService<T extends Plugin> {

  @Value("${ftn.plugins.path}")
  public String path;

  protected final List<T> plugins = new ArrayList<>();

  private final Class<T> pluginClass;

  protected AbstractPluginService(Class<T> pluginClass) {
    this.pluginClass = pluginClass;
  }

  @PostConstruct
  protected void postConstruct() {
    reloadPlugins();
  }

  protected void reloadPlugins() {
    plugins.clear();
    new PluginLoader().load(path, pluginClass, plugins);
  }
}
