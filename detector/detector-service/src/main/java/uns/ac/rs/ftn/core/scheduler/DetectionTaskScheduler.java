package uns.ac.rs.ftn.core.scheduler;

import static org.apache.commons.lang3.BooleanUtils.isFalse;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import uns.ac.rs.ftn.core.entity.DetectionTask;
import uns.ac.rs.ftn.core.repository.DetectionTaskRepository;
import uns.ac.rs.ftn.core.service.DatasetService;
import uns.ac.rs.ftn.core.service.DetectorService;
import uns.ac.rs.ftn.coreinterfaces.port.common.DataAccessPlugin;
import uns.ac.rs.ftn.coreinterfaces.port.common.DocumentMetadata;
import uns.ac.rs.ftn.coreinterfaces.port.detector.DetectionResult;
import uns.ac.rs.ftn.coreinterfaces.port.detector.DetectionTaskStatus;
import uns.ac.rs.ftn.coreinterfaces.port.detector.DetectorPlugin;

@Slf4j
@Component
@RequiredArgsConstructor
public class DetectionTaskScheduler {

  private final RestTemplate restTemplate;

  private final DetectionTaskRepository detectionTaskRepository;

  private final DatasetService datasetService;

  private final DetectorService detectorService;

  private boolean running = false;

  @Value("${services.document-processing.document-endpoint}")
  public String documentEndpoint;

  @Scheduled(cron = "0/10 * * * * *") // every 10 seconds
  public void scheduleTasks() {
    if (running) {
      return;
    }
    running = true;
    List<DetectionTask> tasks = detectionTaskRepository.findAllByStatus(DetectionTaskStatus.PENDING);
    tasks.forEach(task -> task.setStatus(DetectionTaskStatus.RUNNING));
    detectionTaskRepository.saveAll(tasks);
    tasks.addAll(detectionTaskRepository.findAllByStatus(DetectionTaskStatus.RUNNING));

    tasks.forEach(this::processTask);
    running = false;
  }

  private void processTask(DetectionTask detectionTask) {
    try {
      ResponseEntity<DocumentMetadata> responseEntity = restTemplate
        .exchange(documentEndpoint + "/" + detectionTask.getDocumentId(), HttpMethod.GET, null, DocumentMetadata.class);
      DocumentMetadata documentMetadata = responseEntity.getBody();
      if (documentMetadata == null) {
        return;
      }
      if (isFalse(documentMetadata.getProcessingDone())) {
        return;
      }

      DataAccessPlugin dataAccessPlugin = datasetService.getDataAccessorByName(detectionTask.getDatasetName());
      DetectorPlugin detectorPlugin = detectorService.getDetectorByName(detectionTask.getDetectorName());

      DetectionResult result = detectorPlugin.detect(dataAccessPlugin.loadDataset(documentMetadata));
      detectionTask.setDetectionResult(result);
      detectionTask.setStatus(DetectionTaskStatus.COMPLETED);
      detectionTaskRepository.save(detectionTask);
    } catch (Exception e) {
      log.error("Exception: ", e);
    }
  }
}
