package uns.ac.rs.ftn.core.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import uns.ac.rs.ftn.coreinterfaces.port.detector.DetectionResult;
import uns.ac.rs.ftn.coreinterfaces.port.detector.DetectionTaskStatus;

@Data
@AllArgsConstructor
@Document(collection = "detection-task")
public class DetectionTask {

  @Id
  private String id;

  private String documentId;

  private String detectorName;

  private String datasetName;

  private DetectionTaskStatus status;

  private DetectionResult detectionResult;

  private String creatorId;
}
