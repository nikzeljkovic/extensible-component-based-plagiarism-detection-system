package uns.ac.rs.ftn.core.service;

import java.util.List;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import rs.ac.uns.ftn.plag.restclient.clients.DocumentRestClient;
import rs.ac.uns.ftn.springcommonrestexceptions.exception.NotFoundException;
import uns.ac.rs.ftn.core.entity.DetectionTask;
import uns.ac.rs.ftn.core.repository.DetectionTaskRepository;
import uns.ac.rs.ftn.coreinterfaces.port.detector.DetectionTaskStatus;

@Service
@RequiredArgsConstructor
public class DetectionTaskService {

  private final DetectionTaskRepository detectionTaskRepository;

  private final DocumentRestClient documentRestClient;

  public DetectionTask createDetectionTask(DetectionTask detectionTask) {
    verifyDocumentExists(detectionTask.getDocumentId());
    detectionTask.setCreatorId(getCurrentUserId());
    detectionTask.setStatus(DetectionTaskStatus.PENDING);
    return detectionTaskRepository.save(detectionTask);
  }

  public List<DetectionTask> findAll() {
    return detectionTaskRepository.findAllByCreatorId(getCurrentUserId());
  }

  public DetectionTask findById(String id) {
    return detectionTaskRepository.findByIdAndCreatorId(id, getCurrentUserId())
      .orElseThrow(() -> new NotFoundException("Detection task with the given id was not found."));
  }

  private void verifyDocumentExists(String documentId) {
    documentRestClient.findById(documentId)
      .orElseThrow(() -> new NotFoundException("Document with given id not found."));
  }

  private String getCurrentUserId() {
    return (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
  }
}
