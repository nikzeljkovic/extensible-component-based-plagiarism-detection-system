package uns.ac.rs.ftn.core.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import uns.ac.rs.ftn.core.service.DatasetService;
import uns.ac.rs.ftn.coreinterfaces.port.common.PluginMetadata;

@RestController
@RequestMapping("/api/v1/datasets")
@RequiredArgsConstructor
public class DatasetsController {

  private final DatasetService datasetService;

  @GetMapping(produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<List<PluginMetadata>> getPlugins() {
    return ResponseEntity.ok(datasetService.getPluginsMetadata());
  }
}
