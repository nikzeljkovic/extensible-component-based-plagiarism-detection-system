package uns.ac.rs.ftn.core.service;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import uns.ac.rs.ftn.coreinterfaces.port.common.DataAccessPlugin;
import uns.ac.rs.ftn.coreinterfaces.port.common.Plugin;
import uns.ac.rs.ftn.coreinterfaces.port.common.PluginMetadata;

@Service
public class DatasetService extends AbstractPluginService<DataAccessPlugin> {

  protected DatasetService() {
    super(DataAccessPlugin.class);
  }

  public List<PluginMetadata> getPluginsMetadata() {
    reloadPlugins();
    return plugins.stream().map(Plugin::getPluginMetadata).collect(Collectors.toList());
  }

  public DataAccessPlugin getDataAccessorByName(String accessorName) {
    return plugins.stream()
      .filter(p -> StringUtils.equals(accessorName, p.getPluginMetadata().getName()))
      .findFirst()
      .orElse(null);
  }
}
