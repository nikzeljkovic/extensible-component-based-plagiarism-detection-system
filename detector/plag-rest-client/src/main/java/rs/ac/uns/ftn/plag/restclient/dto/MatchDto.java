package rs.ac.uns.ftn.plag.restclient.dto;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.commons.lang3.tuple.Pair;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MatchDto {

  private DocumentMetadataDto documentMetadata;

  private Double score;

  private List<StringRangeDto> rangesInCandidateDocument;

  private List<StringRangeDto> rangesInReferenceDocument;

  public List<Pair<String, String>> getSimilarParts() {
    if (rangesInCandidateDocument.size() != rangesInReferenceDocument.size()) {
      return Collections.emptyList();
    }

    return IntStream.range(0, rangesInCandidateDocument.size())
      .mapToObj(i -> Pair.of(rangesInCandidateDocument.get(i).getText(), rangesInReferenceDocument.get(i).getText()))
      .collect(Collectors.toList());
  }
}
