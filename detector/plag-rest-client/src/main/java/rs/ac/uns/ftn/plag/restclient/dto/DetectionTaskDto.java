package rs.ac.uns.ftn.plag.restclient.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DetectionTaskDto {

  private String id;

  private String documentId;

  private String detectorName;

  private String datasetName;

  private String status;

  private DetectionResultDto detectionResult;

  public DetectionTaskDto(String documentId, String detectorName, String datasetName) {
    this.documentId = documentId;
    this.detectorName = detectorName;
    this.datasetName = datasetName;
  }
}
