package rs.ac.uns.ftn.plag.restclient.dto;

import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DocumentMetadataDto {

  private String id;

  private String fileName;

  private String filePath;

  private Boolean processingDone;

  private Map<String, Object> metadata;
}
