package rs.ac.uns.ftn.plag.restclient.clients;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import rs.ac.uns.ftn.plag.restclient.dto.LoginResponse;
import rs.ac.uns.ftn.plag.restclient.dto.UserDto;
import rs.ac.uns.ftn.plag.restclient.port.TokenPersistenceHandler;

@Slf4j
@Component
@RequiredArgsConstructor
public class UserRestClient {

  @Value("${services.auth.user-endpoint}")
  public String userEndpoint;

  @Value("${services.auth.users-endpoint}")
  public String usersEndpoint;

  @Value("${services.auth.login-endpoint}")
  public String loginEndpoint;

  @Nullable
  private final TokenPersistenceHandler tokenPersistenceHandler;

  private final RestTemplate restTemplate;

  public boolean login(UserDto userDto) {
    try {
      LoginResponse loginResponse = restTemplate.postForEntity(loginEndpoint, userDto, LoginResponse.class).getBody();
      if (loginResponse == null) {
        return false;
      }
      saveToken(loginResponse.getToken());
      return true;
    } catch (Exception e) {
      log.error("Could not login: ", e);
    }
    return false;
  }

  private void saveToken(String token) {
    if (tokenPersistenceHandler != null) {
      tokenPersistenceHandler.saveToken(token);
    }
  }

  public Optional<UserDto> register(UserDto userDto) {
    try {
      UserDto response = restTemplate.postForEntity(usersEndpoint, userDto, UserDto.class).getBody();
      return Optional.ofNullable(response);
    } catch (Exception e) {
      log.error("Could not login: ", e);
      return Optional.empty();
    }
  }

  public Optional<UserDto> getLoggedInUser() {
    if (tokenPersistenceHandler == null) {
      return Optional.empty();
    }

    try {
      ResponseEntity<UserDto> response =
          restTemplate.exchange(userEndpoint + "/me", HttpMethod.GET, new HttpEntity<>(getAuthHeader()), UserDto.class);
      return Optional.ofNullable(response.getBody());
    } catch (Exception e) {
      log.error("Exception: ", e);
      return Optional.empty();
    }
  }

  public void logout() {
    saveToken(null);
  }

  public HttpHeaders getAuthHeader() {
    if (tokenPersistenceHandler == null) {
      throw new RuntimeException("Token persistence handler should not be null.");
    }
    HttpHeaders headers = new HttpHeaders();
    headers.add("Authorization", "Bearer " + tokenPersistenceHandler.getToken());
    return headers;
  }
}
