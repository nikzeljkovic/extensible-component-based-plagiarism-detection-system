package rs.ac.uns.ftn.plag.restclient.clients;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import rs.ac.uns.ftn.plag.restclient.dto.PluginDto;

@Slf4j
@Component
@RequiredArgsConstructor
public class DetectorRestClient {

  @Value("${services.detection.detectors-endpoint}")
  public String detectorsEndpoint;

  private final RestTemplate restTemplate;

  private final UserRestClient userRestClient;

  public List<PluginDto> findAll() {
    try {
      ResponseEntity<List<PluginDto>> response = restTemplate.exchange(detectorsEndpoint, HttpMethod.GET,
          new HttpEntity<>(userRestClient.getAuthHeader()), new ParameterizedTypeReference<List<PluginDto>>() {
          });
      List<PluginDto> plugins = response.getBody();
      if (plugins == null) {
        return Collections.emptyList();
      }
      return plugins;
    } catch (Exception e) {
      log.error("Could not get all detectors: ", e);
      return Collections.emptyList();
    }
  }
}
