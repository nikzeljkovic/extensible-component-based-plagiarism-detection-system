package rs.ac.uns.ftn.plag.restclient.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StringRangeDto {

  private int start;

  private int end;

  private String text;
}
