package rs.ac.uns.ftn.plag.restclient.port;

public interface TokenPersistenceHandler {

  void saveToken(String token);

  String getToken();
}
