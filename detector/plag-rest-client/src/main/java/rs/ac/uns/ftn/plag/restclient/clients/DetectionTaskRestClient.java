package rs.ac.uns.ftn.plag.restclient.clients;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import rs.ac.uns.ftn.plag.restclient.dto.DetectionTaskDto;

@Slf4j
@Component
@RequiredArgsConstructor
public class DetectionTaskRestClient {

  @Value("${services.detection.detecton-tasks-endpoint}")
  public String detectionTasksEndpoint;

  @Value("${services.detection.detecton-task-endpoint}")
  public String detectionTaskEndpoint;

  private final RestTemplate restTemplate;

  private final UserRestClient userRestClient;

  public Optional<DetectionTaskDto> create(DetectionTaskDto detectionTaskDto) {
    try {
      ResponseEntity<DetectionTaskDto> response = restTemplate.exchange(detectionTasksEndpoint, HttpMethod.POST,
          new HttpEntity<>(detectionTaskDto, userRestClient.getAuthHeader()), DetectionTaskDto.class);
      DetectionTaskDto responseBody = response.getBody();
      return Optional.ofNullable(responseBody);
    } catch (Exception e) {
      log.error("Could not create task.", e);
      return Optional.empty();
    }
  }

  public List<DetectionTaskDto> findAll() {
    try {
      ResponseEntity<List<DetectionTaskDto>> response = restTemplate.exchange(detectionTasksEndpoint, HttpMethod.GET,
          new HttpEntity<>(userRestClient.getAuthHeader()), new ParameterizedTypeReference<List<DetectionTaskDto>>() {
          });
      List<DetectionTaskDto> body = response.getBody();
      if (body == null) {
        return Collections.emptyList();
      }
      return body;
    } catch (Exception e) {
      log.error("Could not get all tasks: ", e);
      return Collections.emptyList();
    }
  }

  public Optional<DetectionTaskDto> findById(String id) {
    try {
      ResponseEntity<DetectionTaskDto> response = restTemplate.exchange(detectionTaskEndpoint + "/" + id,
          HttpMethod.GET, new HttpEntity<>(userRestClient.getAuthHeader()), DetectionTaskDto.class);
      DetectionTaskDto detectionTaskDto = response.getBody();
      return Optional.ofNullable(detectionTaskDto);
    } catch (Exception e) {
      log.error("Could not find task by id: ", e);
      return Optional.empty();
    }
  }
}
