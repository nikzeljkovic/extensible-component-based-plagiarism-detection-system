package rs.ac.uns.ftn.plag.restclient.clients;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import rs.ac.uns.ftn.plag.restclient.dto.DocumentMetadataDto;

@Slf4j
@Component
@RequiredArgsConstructor
public class DocumentRestClient {

  @Value("${services.document-processing.document-endpoint}")
  public String documentEndpoint;

  @Value("${services.document-processing.documents-endpoint}")
  public String documentsEndpoint;

  private final RestTemplate restTemplate;

  private final UserRestClient userRestClient;

  public Optional<DocumentMetadataDto> findById(String id) {
    try {
      ResponseEntity<DocumentMetadataDto> response =
          restTemplate.getForEntity(documentEndpoint + "/" + id, DocumentMetadataDto.class);
      DocumentMetadataDto dto = response.getBody();
      return Optional.ofNullable(dto);
    } catch (Exception e) {
      log.error("Could not get document by id: ", e);
      return Optional.empty();
    }
  }

  public List<DocumentMetadataDto> findAll() {
    try {
      ResponseEntity<List<DocumentMetadataDto>> response = restTemplate.exchange(documentsEndpoint, HttpMethod.GET,
          HttpEntity.EMPTY, new ParameterizedTypeReference<List<DocumentMetadataDto>>() {
          });
      List<DocumentMetadataDto> documents = response.getBody();
      if (documents == null) {
        return Collections.emptyList();
      }
      return documents;
    } catch (Exception e) {
      log.error("Could not get all documents: ", e);
      return Collections.emptyList();
    }
  }

  public List<DocumentMetadataDto> uploadDocument(File file, String name, Boolean addToReferenceSet) {
    if (StringUtils.isBlank(name)) {
      name = file.getName();
    }
    try {
      MultiValueMap<String, Object> formData = new LinkedMultiValueMap<>();
      formData.add("file", new FileSystemResource(file));
      formData.add("name", name);
      formData.add("addToReferenceSet", addToReferenceSet.toString());

      HttpHeaders headers = new HttpHeaders();
      headers.setContentType(MediaType.MULTIPART_FORM_DATA);
      headers.addAll(userRestClient.getAuthHeader());

      HttpEntity<Object> requestEntity = new HttpEntity<>(formData, headers);
      ResponseEntity<List<DocumentMetadataDto>> responseEntity = restTemplate.exchange(documentsEndpoint,
          HttpMethod.POST, requestEntity, new ParameterizedTypeReference<List<DocumentMetadataDto>>() {
          }, Collections.emptyMap());

      List<DocumentMetadataDto> result = responseEntity.getBody();
      if (result == null) {
        return Collections.emptyList();
      }
      return result;
    } catch (Exception e) {
      log.error("Could not upload document: ", e);
      return Collections.emptyList();
    }
  }
}
