package rs.ac.uns.ftn.plag.restclient.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PluginDto {

  private String name;

  private String humanReadableName;

  private String description;

  private List<String> requiredDocumentMetadata;

  private List<String> dependencies;
}
