package rs.ac.uns.ftn.plag.restclient.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DetectionResultDto {

  private List<MatchDto> matches;
}
