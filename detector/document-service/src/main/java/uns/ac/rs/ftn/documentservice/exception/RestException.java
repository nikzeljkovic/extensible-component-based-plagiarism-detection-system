package uns.ac.rs.ftn.documentservice.exception;

import org.springframework.http.HttpStatus;

import lombok.Getter;

@Getter
public class RestException extends RuntimeException {

  private final HttpStatus httpStatus;

  public RestException(HttpStatus httpStatus, String message) {
    super(message);
    this.httpStatus = httpStatus;
  }
}
