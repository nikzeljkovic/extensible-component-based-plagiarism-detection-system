package uns.ac.rs.ftn.documentservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
@SpringBootApplication
public class DocumentServerApp {

  public static void main(String[] args) {
    SpringApplication.run(DocumentServerApp.class, args);
  }
}
