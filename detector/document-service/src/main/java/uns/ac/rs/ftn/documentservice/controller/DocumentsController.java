package uns.ac.rs.ftn.documentservice.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import lombok.RequiredArgsConstructor;
import uns.ac.rs.ftn.documentservice.dto.DocumentUpload;
import uns.ac.rs.ftn.documentservice.entity.DocumentMetadata;
import uns.ac.rs.ftn.documentservice.service.DocumentService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/documents")
public class DocumentsController {

  private final DocumentService documentService;

  @PostMapping(produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<List<DocumentMetadata>> create(@ModelAttribute DocumentUpload documentUpload) {
    return ResponseEntity.ok(documentService.create(documentUpload));
  }

  @GetMapping(produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<List<DocumentMetadata>> get(
      @RequestParam(value = "partOfReferenceSet", required = false) Boolean partOfReferenceSet) {
    return ResponseEntity.ok(documentService.findAll(partOfReferenceSet));
  }
}
