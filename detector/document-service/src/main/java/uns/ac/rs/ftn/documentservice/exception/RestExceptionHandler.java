package uns.ac.rs.ftn.documentservice.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import uns.ac.rs.ftn.documentservice.dto.ApiError;

@ControllerAdvice
public class RestExceptionHandler {

  @ExceptionHandler(RestException.class)
  public ResponseEntity<ApiError> handleNotFoundException(RestException exception) {
    return new ResponseEntity<>(new ApiError(exception.getMessage()), exception.getHttpStatus());
  }
}
