package uns.ac.rs.ftn.documentservice.dto;

import org.springframework.web.multipart.MultipartFile;

import lombok.Data;

@Data
public class DocumentUpload {

  private MultipartFile[] file;

  private String name;

  private boolean addToReferenceSet;
}
