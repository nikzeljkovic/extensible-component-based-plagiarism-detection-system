package uns.ac.rs.ftn.documentservice.service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import uns.ac.rs.ftn.coreinterfaces.port.common.CommonMetadataFields;
import uns.ac.rs.ftn.coreinterfaces.port.processing.ProcessingPlugin;
import uns.ac.rs.ftn.documentservice.dto.DocumentUpload;
import uns.ac.rs.ftn.documentservice.entity.DocumentMetadata;
import uns.ac.rs.ftn.documentservice.exception.NotFoundException;
import uns.ac.rs.ftn.documentservice.repository.DocumentMetadataRepository;
import uns.ac.rs.ftn.documentservice.repository.DocumentRepository;
import uns.ac.rs.ftn.documentservice.util.AsyncRunner;
import uns.ac.rs.ftn.pluginutils.PluginLoader;

@Service
@Slf4j
@RequiredArgsConstructor
public class DocumentService {

  @Value("${ftn.plugins.path}")
  public String path;

  private final AsyncRunner asyncRunner;

  private final DocumentMetadataRepository documentMetadataRepository;

  private final DocumentRepository documentRepository;

  private final List<ProcessingPlugin> processors = new ArrayList<>();

  @PostConstruct
  protected void postConstruct() {
    loadPlugins();
    // documentMetadataRepository.findAll().forEach(doc -> {
    // documentRepository.delete(doc.getFilePath());
    // documentRepository.delete(doc.get(CommonMetadataFields.FULL_TEXT_PATH));
    // });
    // documentMetadataRepository.deleteAll();
  }

  private void loadPlugins() {
    new PluginLoader().load(path, ProcessingPlugin.class, processors);
    log.info("Loaded processors: " + processors);
  }

  public void process(DocumentMetadata documentMetadata) {
    asyncRunner.run(() -> {
      try {
        processors.forEach(p -> p.process(map(documentMetadata),
            processingResult -> documentMetadata.set(p.getResultFieldName(), processingResult)));
        setFileSizeMetadata(documentMetadata);
        documentMetadata.setProcessingDone(true);
        documentMetadataRepository.save(documentMetadata);
      } catch (Exception e) {
        log.error("Processing failed: ", e);
        documentMetadata.setProcessingDone(true);
        documentMetadataRepository.save(documentMetadata);
      }
    });
  }

  private void setFileSizeMetadata(DocumentMetadata documentMetadata) {
    if (StringUtils.hasLength(documentMetadata.getFilePath())) {
      documentMetadata.set(CommonMetadataFields.FILE_SIZE_BYTES,
          FileUtils.sizeOf(new File(documentMetadata.getFilePath())));
    }
    if (StringUtils.hasLength(documentMetadata.getFilePath())) {
      documentMetadata.set(CommonMetadataFields.FULL_TEXT_SIZE_BYTES,
          FileUtils.sizeOf(new File((String) documentMetadata.get(CommonMetadataFields.FULL_TEXT_PATH))));
    }
  }

  private uns.ac.rs.ftn.coreinterfaces.port.common.DocumentMetadata map(DocumentMetadata documentMetadata) {
    return new uns.ac.rs.ftn.coreinterfaces.port.common.DocumentMetadata(documentMetadata.getFileName(),
        documentMetadata.getFilePath(), documentMetadata.getMetadata());
  }

  public List<DocumentMetadata> create(DocumentUpload documentUpload) {
    List<DocumentMetadata> results = new ArrayList<>();
    for (MultipartFile multipartFile : documentUpload.getFile()) {
      DocumentMetadata documentMetadata = documentRepository.create(multipartFile);
      documentMetadata.setPartOfReferenceSet(documentUpload.isAddToReferenceSet());
      if (documentUpload.getName() != null) {
        // Overwrite original filename.
        documentMetadata.setFileName(documentUpload.getName());
      }

      documentMetadata = documentMetadataRepository.save(documentMetadata);
      results.add(documentMetadata);
      process(documentMetadata);
    }

    return results;
  }

  public List<DocumentMetadata> findAll(Boolean partOfReferenceSet) {
    if (partOfReferenceSet == null) {
      return documentMetadataRepository.findAll();
    }
    return documentMetadataRepository.findByPartOfReferenceSet(partOfReferenceSet);
  }

  public DocumentMetadata findById(String id) {
    return documentMetadataRepository.findById(id).orElseThrow(() -> new NotFoundException("Document not found."));
  }
}
