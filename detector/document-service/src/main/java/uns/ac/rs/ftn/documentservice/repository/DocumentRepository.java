package uns.ac.rs.ftn.documentservice.repository;

import static uns.ac.rs.ftn.coreinterfaces.port.common.CommonMetadataFields.EXTENSION;
import static uns.ac.rs.ftn.coreinterfaces.port.common.CommonMetadataFields.MIMETYPE;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.annotation.PostConstruct;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.tika.Tika;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import lombok.extern.slf4j.Slf4j;
import uns.ac.rs.ftn.documentservice.entity.DocumentMetadata;
import uns.ac.rs.ftn.documentservice.exception.BadRequestException;
import uns.ac.rs.ftn.documentservice.exception.InternalServerException;
import uns.ac.rs.ftn.documentservice.exception.NotFoundException;

@Slf4j
@Component
public class DocumentRepository {

  private Path documentsStoragePath;

  private final Tika tika = new Tika();

  @PostConstruct
  public void init() {
    documentsStoragePath = Paths.get("documents").toAbsolutePath().normalize();
    // try {
    // FileUtils.deleteDirectory(documentsStoragePath.toFile());
    // Files.createDirectories(documentsStoragePath);
    // } catch (final IOException e) {
    // log.error("Could not create documents storage directory.", e);
    // throw new InternalServerException("Could not create documents storage directory.");
    // }
  }

  public DocumentMetadata create(MultipartFile file) {
    try {
      String extension = FilenameUtils.getExtension(file.getOriginalFilename());
      String storedFileName = UUID.randomUUID().toString() + "." + extension;
      Path targetLocation = documentsStoragePath.resolve(storedFileName);
      Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

      Map<String, Object> processingInfo = new HashMap<>();
      processingInfo.put(MIMETYPE, getMimeType(targetLocation));
      processingInfo.put(EXTENSION, extension);

      return new DocumentMetadata(null, file.getOriginalFilename(), storedFileName,
          targetLocation.toAbsolutePath().toString(), false, false, processingInfo);
    } catch (Exception e) {
      log.error("Could not save file.", e);
      throw new InternalServerException("Could not save file!");
    }
  }

  private String getMimeType(Path targetLocation) {
    try {
      return tika.detect(targetLocation);
    } catch (Exception e) {
      log.error("Could not detect mimeType using Tika.", e);
      return null;
    }
  }

  public File findFileByStoredFileName(String storedFileName) {
    try {
      final Path filePath = documentsStoragePath.resolve(storedFileName).normalize();

      return filePath.toFile();
    } catch (Exception e) {
      throw new NotFoundException("File with stored file name not found!");
    }
  }

  public Resource findResourceByStoredFileName(DocumentMetadata documentMetadata) {
    try {
      final Path filePath = documentsStoragePath.resolve(documentMetadata.getStoredFileName()).normalize();
      final Resource resource = new UrlResource(filePath.toUri());
      if (!resource.exists()) {
        throw new BadRequestException("Document with given filename is not stored on the server.");
      }
      return resource;
    } catch (final MalformedURLException e) {
      throw new InternalServerException("The url of the document is malformed.");
    }
  }

  public void delete(String path) {
    if (!StringUtils.hasLength(path)) {
      return;
    }
    try {
      File file = new File(path);
      if (file.exists()) {
        FileUtils.delete(file);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}