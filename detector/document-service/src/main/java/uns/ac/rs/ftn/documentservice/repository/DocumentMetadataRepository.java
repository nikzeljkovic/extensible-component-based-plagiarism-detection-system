package uns.ac.rs.ftn.documentservice.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import uns.ac.rs.ftn.documentservice.entity.DocumentMetadata;

public interface DocumentMetadataRepository extends MongoRepository<DocumentMetadata, String> {

  List<DocumentMetadata> findByPartOfReferenceSet(boolean partOfReferenceSet);
}
