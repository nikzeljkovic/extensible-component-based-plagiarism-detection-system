package uns.ac.rs.ftn.documentservice.entity;

import java.util.HashMap;
import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@Document(collection = "document-metadata")
public class DocumentMetadata {

  @Id
  private String id;

  private String fileName;

  private String storedFileName;

  private String filePath;

  private Boolean partOfReferenceSet;

  private Boolean processingDone;

  private Map<String, Object> metadata;

  public void set(String key, Object value) {
    if (metadata == null) {
      metadata = new HashMap<>();
    }
    metadata.put(key, value);
  }

  public <T> T get(String processingInfoField) {
    try {
      return (T) metadata.get(processingInfoField);
    } catch (ClassCastException e) {
      return null;
    }
  }
}
