package uns.ac.rs.ftn.documentservice.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import uns.ac.rs.ftn.documentservice.entity.DocumentMetadata;
import uns.ac.rs.ftn.documentservice.service.DocumentService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/document/{id}")
public class DocumentController {

  private final DocumentService documentService;

  @GetMapping(produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<DocumentMetadata> get(@PathVariable String id) {
    return ResponseEntity.ok(documentService.findById(id));
  }
}
