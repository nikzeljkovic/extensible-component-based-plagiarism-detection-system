package uns.ac.rs.ftn.documentservice.util;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class AsyncRunner {

  @Async
  public void run(Runnable runnable) {
    runnable.run();
  }
}
