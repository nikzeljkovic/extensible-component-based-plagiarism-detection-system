package rs.ac.uns.ftn.plag.frontend.views.datasets;

import com.vaadin.flow.component.dependency.Uses;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

import rs.ac.uns.ftn.plag.frontend.config.LoggedInAllowed;
import rs.ac.uns.ftn.plag.frontend.views.MainLayout;
import rs.ac.uns.ftn.plag.frontend.views.pluginscommon.PluginListLayout;
import rs.ac.uns.ftn.plag.restclient.clients.DatasetRestClient;

@PageTitle("Datasets")
@Route(value = "datasets", layout = MainLayout.class)
@LoggedInAllowed
@Uses(Icon.class)
public class DatasetsView extends Div {

  private final PluginListLayout grid = new PluginListLayout();

  public DatasetsView(DatasetRestClient datasetRestClient) {
    addClassNames("detectiontasks-view", "flex", "flex-col", "h-full");

    VerticalLayout wrapperLayout = new VerticalLayout();
    wrapperLayout.setSizeFull();
    wrapperLayout.add(grid);
    add(wrapperLayout);

    grid.setItems(datasetRestClient.findAll());
  }
}
