package rs.ac.uns.ftn.plag.frontend.views;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.avatar.Avatar;
import com.vaadin.flow.component.contextmenu.ContextMenu;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.tabs.TabsVariant;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.RouterLink;
import com.vaadin.flow.server.auth.AccessAnnotationChecker;

import rs.ac.uns.ftn.plag.frontend.views.datasets.DatasetsView;
import rs.ac.uns.ftn.plag.frontend.views.detectiontasks.DetectiontasksView;
import rs.ac.uns.ftn.plag.frontend.views.detectors.DetectorsView;
import rs.ac.uns.ftn.plag.frontend.views.documentsview.DocumentsView;
import rs.ac.uns.ftn.plag.frontend.views.login.LoginView;
import rs.ac.uns.ftn.plag.frontend.views.register.RegisterView;
import rs.ac.uns.ftn.plag.restclient.clients.UserRestClient;
import rs.ac.uns.ftn.plag.restclient.dto.UserDto;

/**
 * The main view is a top-level placeholder for other views.
 */
@PageTitle("Main")
public class MainLayout extends AppLayout {

  public static final String USER_IMAGE_PLACEHOLDER =
      "https://www.pngfind.com/pngs/m/610-6104451_image-placeholder-png-user-profile-placeholder-image-png.png";

  public static class MenuItemInfo {

    private final String text;
    private final String iconClass;
    private final Class<? extends Component> view;

    public MenuItemInfo(String text, String iconClass, Class<? extends Component> view) {
      this.text = text;
      this.iconClass = iconClass;
      this.view = view;
    }

    public String getText() {
      return text;
    }

    public String getIconClass() {
      return iconClass;
    }

    public Class<? extends Component> getView() {
      return view;
    }

  }

  private final Tabs menu;
  private H1 viewTitle;

  private final AccessAnnotationChecker accessChecker;

  private final UserRestClient userRestClient;

  public MainLayout(UserRestClient userRestClient, AccessAnnotationChecker accessChecker) {
    this.userRestClient = userRestClient;
    this.accessChecker = accessChecker;

    setPrimarySection(Section.DRAWER);
    addToNavbar(true, createHeaderContent());
    menu = createMenu();
    addToDrawer(createDrawerContent(menu));
  }

  private Component createHeaderContent() {
    HorizontalLayout layout = new HorizontalLayout();
    layout.setClassName("sidemenu-header");
    layout.getThemeList().set("dark", true);
    layout.setWidthFull();
    layout.setSpacing(false);
    layout.setAlignItems(FlexComponent.Alignment.CENTER);
    layout.add(new DrawerToggle());
    viewTitle = new H1();
    layout.add(viewTitle);

    Optional<UserDto> maybeUser = userRestClient.getLoggedInUser();
    if (maybeUser.isPresent()) {
      UserDto user = maybeUser.get();
      Avatar avatar = new Avatar(user.getEmail(), USER_IMAGE_PLACEHOLDER);
      avatar.addClassNames("ms-auto", "me-m");
      ContextMenu userMenu = new ContextMenu(avatar);
      userMenu.setOpenOnClick(true);
      userMenu.addItem("Logout", e -> {
        userRestClient.logout();
        UI.getCurrent().getPage().setLocation("/login");
      });
      layout.add(avatar);
    } else {
      Anchor loginLink = new Anchor("login", "Sign in");
      loginLink.addClassNames("ms-auto", "me-m");
      layout.add(loginLink);
    }

    return layout;
  }

  private Component createDrawerContent(Tabs menu) {
    VerticalLayout layout = new VerticalLayout();
    layout.setClassName("sidemenu-menu");
    layout.setSizeFull();
    layout.setPadding(false);
    layout.setSpacing(false);
    layout.getThemeList().set("spacing-s", true);
    layout.setAlignItems(FlexComponent.Alignment.STRETCH);
    HorizontalLayout logoLayout = new HorizontalLayout();
    logoLayout.setId("logo");
    logoLayout.setAlignItems(FlexComponent.Alignment.CENTER);
    logoLayout.add(new Image("images/logo.png", "Plugin Plagiarism Detector logo"));
    logoLayout.add(new H1("Plugin Plagiarism Detector"));
    layout.add(logoLayout, menu);
    return layout;
  }

  private Tabs createMenu() {
    final Tabs tabs = new Tabs();
    tabs.setOrientation(Tabs.Orientation.VERTICAL);
    tabs.addThemeVariants(TabsVariant.LUMO_MINIMAL);
    tabs.setId("tabs");
    for (Tab menuTab : createMenuItems()) {
      tabs.add(menuTab);
    }
    return tabs;
  }

  private List<Tab> createMenuItems() {
    MenuItemInfo[] menuItems = new MenuItemInfo[] { //
        new MenuItemInfo("Register", "la la-user", RegisterView.class), //

        new MenuItemInfo("Login", "la la-user", LoginView.class), //

        new MenuItemInfo("Documents", "la la-list", DocumentsView.class), //

        new MenuItemInfo("Detectors", "la la-list", DetectorsView.class), //

        new MenuItemInfo("Datasets", "la la-list", DatasetsView.class), //

        new MenuItemInfo("Detection tasks", "la la-columns", DetectiontasksView.class), //

    };
    List<Tab> tabs = new ArrayList<>();
    for (MenuItemInfo menuItemInfo : menuItems) {
      if (accessChecker.hasAccess(menuItemInfo.getView())) {
        tabs.add(createTab(menuItemInfo));
      }

    }
    return tabs;
  }

  private static Tab createTab(MenuItemInfo menuItemInfo) {
    Tab tab = new Tab();
    RouterLink link = new RouterLink();
    link.setRoute(menuItemInfo.getView());
    Span iconElement = new Span();
    iconElement.addClassNames("text-l", "pr-s");
    if (!menuItemInfo.getIconClass().isEmpty()) {
      iconElement.addClassNames(menuItemInfo.getIconClass());
    }
    link.add(iconElement, new Text(menuItemInfo.getText()));
    tab.add(link);
    ComponentUtil.setData(tab, Class.class, menuItemInfo.getView());
    return tab;
  }

  @Override
  protected void afterNavigation() {
    super.afterNavigation();
    getTabForComponent(getContent()).ifPresent(menu::setSelectedTab);
    viewTitle.setText(getCurrentPageTitle());
  }

  private Optional<Tab> getTabForComponent(Component component) {
    return menu.getChildren()
      .filter(tab -> ComponentUtil.getData(tab, Class.class).equals(component.getClass()))
      .findFirst()
      .map(Tab.class::cast);
  }

  private String getCurrentPageTitle() {
    PageTitle title = getContent().getClass().getAnnotation(PageTitle.class);
    return title == null ? "" : title.value();
  }
}
