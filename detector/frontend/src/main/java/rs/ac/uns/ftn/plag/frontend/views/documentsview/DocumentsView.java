package rs.ac.uns.ftn.plag.frontend.views.documentsview;

import static java.util.UUID.randomUUID;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import com.vaadin.flow.component.dependency.Uses;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.upload.Receiver;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.FileData;
import com.vaadin.flow.component.upload.receivers.UploadOutputStream;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

import lombok.extern.slf4j.Slf4j;
import rs.ac.uns.ftn.plag.frontend.config.LoggedInAllowed;
import rs.ac.uns.ftn.plag.frontend.config.TempFileConfig;
import rs.ac.uns.ftn.plag.frontend.views.MainLayout;
import rs.ac.uns.ftn.plag.frontend.views.pluginscommon.AllItemsGrid;
import rs.ac.uns.ftn.plag.frontend.views.pluginscommon.DocumentViewModel;
import rs.ac.uns.ftn.plag.restclient.clients.DocumentRestClient;
import rs.ac.uns.ftn.plag.restclient.dto.DocumentMetadataDto;
import uns.ac.rs.ftn.coreinterfaces.port.common.CommonMetadataFields;

@Slf4j
@PageTitle("Documents")
@Route(value = "documents", layout = MainLayout.class)
@LoggedInAllowed
@Uses(Icon.class)
public class DocumentsView extends Div {

  private final DocumentRestClient documentRestClient;

  private final TempFileConfig tempFileConfig;
  private final AllItemsGrid<DocumentViewModel> grid;

  public DocumentsView(DocumentRestClient documentRestClient, TempFileConfig tempFileConfig) {
    this.documentRestClient = documentRestClient;
    this.tempFileConfig = tempFileConfig;

    addClassNames("detectiontasks-view", "flex", "flex-col", "h-full");

    VerticalLayout wrapperLayout = new VerticalLayout();
    wrapperLayout.setSizeFull();
    grid = new AllItemsGrid<>(DocumentViewModel.class);

    wrapperLayout.add();
    wrapperLayout.add(getFileUpload());
    wrapperLayout.add(grid);
    add(wrapperLayout);

    loadItems();
  }

  private void loadItems() {
    grid.setItems(documentRestClient.findAll().stream().map(i -> {
      String mimeType = (String) i.getMetadata().get(CommonMetadataFields.MIMETYPE);
      String md5 = (String) i.getMetadata().get(CommonMetadataFields.MD5);
      return new DocumentViewModel(i.getId(), i.getFileName(), i.getProcessingDone(), mimeType, md5);
    }).collect(Collectors.toList()));
  }

  private VerticalLayout getFileUpload() {
    String tempFile = tempFileConfig.resolve(randomUUID().toString()).toAbsolutePath().toString();
    TextField textField = new TextField("Document name", "Optional");
    UploadBuffer uploadBuffer = new UploadBuffer(tempFile);
    Upload upload = new Upload(uploadBuffer);
    upload.setAutoUpload(true);

    upload.addFinishedListener(event -> {
      String fileName =
          StringUtils.isNotBlank(textField.getValue()) ? textField.getValue() : uploadBuffer.getOriginalFileName();
      List<DocumentMetadataDto> result = documentRestClient.uploadDocument(uploadBuffer.getFile(), fileName, false);
      if (result.isEmpty()) {
        Notification.show("Could not upload file.");
      } else {
        Notification.show("File " + event.getFileName() + " uploaded.");
        loadItems();
      }
      try {
        FileUtils.forceDelete(new File(tempFile));
      } catch (Exception e) {
        log.error("Could not delete temp file: ", e);
      }
    });

    upload.addFileRejectedListener(event -> Notification.show("Failed to upload file"));

    return new VerticalLayout(textField, upload);
  }

  @Slf4j
  public static class UploadBuffer implements Receiver {

    private FileData file;

    private final String tempFileName;

    private String originalFileName;

    public UploadBuffer(String tempFileName) {
      this.tempFileName = tempFileName;
    }

    @Override
    public OutputStream receiveUpload(String fileName, String mimeType) {
      this.originalFileName = fileName;
      try {
        UploadOutputStream outputBuffer = new UploadOutputStream(new File(tempFileName));
        file = new FileData(fileName, mimeType, outputBuffer);
        return outputBuffer;
      } catch (FileNotFoundException e) {
        log.error("Could not save file to temp file: ", e);
        return null;
      }
    }

    public File getFile() {
      return file.getFile();
    }

    public String getOriginalFileName() {
      return originalFileName;
    }
  }
}
