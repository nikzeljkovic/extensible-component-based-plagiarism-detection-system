package rs.ac.uns.ftn.plag.frontend.views.detectiontasks;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;

import rs.ac.uns.ftn.plag.restclient.dto.DetectionResultDto;

public class ResultsDetailView extends VerticalLayout {

  public ResultsDetailView(DetectionResultDto resultDto) {
    resultDto.getMatches().stream().map(MatchDetailView::new).forEach(this::add);
  }
}
