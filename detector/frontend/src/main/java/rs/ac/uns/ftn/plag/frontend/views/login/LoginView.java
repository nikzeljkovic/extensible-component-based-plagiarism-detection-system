package rs.ac.uns.ftn.plag.frontend.views.login;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.login.LoginI18n;
import com.vaadin.flow.component.login.LoginOverlay;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.auth.AnonymousAllowed;

import rs.ac.uns.ftn.plag.frontend.config.OnlyLoggedOutAllowed;
import rs.ac.uns.ftn.plag.restclient.clients.UserRestClient;
import rs.ac.uns.ftn.plag.restclient.dto.UserDto;

@PageTitle("Login")
@AnonymousAllowed
@OnlyLoggedOutAllowed
@Route(value = "login")
public class LoginView extends LoginOverlay {

  public LoginView(UserRestClient userRestClient) {
    setAction("login");

    LoginI18n i18n = LoginI18n.createDefault();
    i18n.setHeader(new LoginI18n.Header());
    i18n.getHeader().setTitle("Plugin Plagiarism Detector");
    i18n.setAdditionalInformation(null);
    setI18n(i18n);

    setForgotPasswordButtonVisible(false);
    setOpened(true);

    addLoginListener(loginEvent -> {
      boolean success = userRestClient.login(new UserDto(loginEvent.getUsername(), loginEvent.getPassword()));
      if (success) {
        Notification.show("Login successful.");
        UI.getCurrent().getPage().setLocation("/tasks");
      }
    });
  }
}
