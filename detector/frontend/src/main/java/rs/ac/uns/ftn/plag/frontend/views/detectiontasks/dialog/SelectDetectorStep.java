package rs.ac.uns.ftn.plag.frontend.views.detectiontasks.dialog;

import java.util.stream.Collectors;

import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

import rs.ac.uns.ftn.plag.frontend.views.pluginscommon.AllItemsGrid;
import rs.ac.uns.ftn.plag.frontend.views.pluginscommon.PluginViewModel;
import rs.ac.uns.ftn.plag.restclient.clients.DetectorRestClient;
import rs.ac.uns.ftn.plag.restclient.dto.DetectionTaskDto;

public class SelectDetectorStep extends DialogStep<DetectionTaskDto> {

  private PluginViewModel pluginViewModel;

  public SelectDetectorStep(DetectorRestClient detectorRestClient) {
    addClassNames("detectiontasks-view", "flex", "flex-col", "h-full");

    VerticalLayout wrapperLayout = new VerticalLayout();
    wrapperLayout.setSizeFull();
    AllItemsGrid<PluginViewModel> grid = new AllItemsGrid<>(PluginViewModel.class);
    wrapperLayout.add(grid);
    add(wrapperLayout);

    grid
      .setItems(detectorRestClient.findAll().stream().map(PluginViewModel::fromPluginDto).collect(Collectors.toList()));

    grid.asSingleSelect().addValueChangeListener(event -> pluginViewModel = event.getValue());

    setHeight(600f, Unit.PIXELS);
    setWidthFull();
  }

  @Override
  public void fillResultDto(DetectionTaskDto resultDto) {
    if (pluginViewModel == null) {
      return;
    }
    resultDto.setDetectorName(pluginViewModel.getName());
  }

  @Override
  public String getStepDescription() {
    return "Select a detector";
  }

  @Override
  public DialogStep<DetectionTaskDto> getNextStep() {
    return null;
  }

  @Override
  public boolean isValid() {
    return pluginViewModel != null;
  }

  @Override
  public String getInvalidMessage() {
    return "Please select a detector";
  }
}
