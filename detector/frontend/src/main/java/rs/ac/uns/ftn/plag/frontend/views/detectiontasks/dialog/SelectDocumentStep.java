package rs.ac.uns.ftn.plag.frontend.views.detectiontasks.dialog;

import java.util.stream.Collectors;

import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

import rs.ac.uns.ftn.plag.frontend.views.pluginscommon.AllItemsGrid;
import rs.ac.uns.ftn.plag.frontend.views.pluginscommon.DocumentViewModel;
import rs.ac.uns.ftn.plag.restclient.clients.DatasetRestClient;
import rs.ac.uns.ftn.plag.restclient.clients.DetectorRestClient;
import rs.ac.uns.ftn.plag.restclient.clients.DocumentRestClient;
import rs.ac.uns.ftn.plag.restclient.dto.DetectionTaskDto;
import uns.ac.rs.ftn.coreinterfaces.port.common.CommonMetadataFields;

public class SelectDocumentStep extends DialogStep<DetectionTaskDto> {

  private final DatasetRestClient datasetRestClient;

  private final DetectorRestClient detectorRestClient;

  private DocumentViewModel documentViewModel;

  public SelectDocumentStep(DocumentRestClient documentRestClient, DatasetRestClient datasetRestClient,
      DetectorRestClient detectorRestClient) {
    this.datasetRestClient = datasetRestClient;
    this.detectorRestClient = detectorRestClient;
    addClassNames("detectiontasks-view", "flex", "flex-col", "h-full");

    VerticalLayout wrapperLayout = new VerticalLayout();
    wrapperLayout.setSizeFull();
    AllItemsGrid<DocumentViewModel> grid = new AllItemsGrid<>(DocumentViewModel.class);
    wrapperLayout.add(grid);
    add(wrapperLayout);

    grid.setItems(documentRestClient.findAll().stream().map(i -> {
      String mimeType = (String) i.getMetadata().get(CommonMetadataFields.MIMETYPE);
      String md5 = (String) i.getMetadata().get(CommonMetadataFields.MD5);
      return new DocumentViewModel(i.getId(), i.getFileName(), i.getProcessingDone(), mimeType, md5);
    }).collect(Collectors.toList()));

    grid.asSingleSelect().addValueChangeListener(event -> {
      documentViewModel = event.getValue();
    });

    setHeight(600f, Unit.PIXELS);
    setWidthFull();
  }

  public String getSelectedDocumentId() {
    if (documentViewModel == null) {
      return null;
    }
    return documentViewModel.getId();
  }

  @Override
  public void fillResultDto(DetectionTaskDto resultDto) {
    resultDto.setDocumentId(getSelectedDocumentId());
  }

  @Override
  public String getStepDescription() {
    return "Select a document for detection";
  }

  @Override
  public DialogStep<DetectionTaskDto> getNextStep() {
    return new SelectDatasetStep(datasetRestClient, detectorRestClient);
  }

  @Override
  public boolean isValid() {
    return documentViewModel != null;
  }

  @Override
  public String getInvalidMessage() {
    return "Please select a document";
  }
}
