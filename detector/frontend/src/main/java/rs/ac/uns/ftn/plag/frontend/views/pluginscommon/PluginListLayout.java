package rs.ac.uns.ftn.plag.frontend.views.pluginscommon;

import java.util.List;
import java.util.stream.Collectors;

import rs.ac.uns.ftn.plag.restclient.dto.PluginDto;

public class PluginListLayout extends AllItemsGrid<PluginViewModel> {

  public PluginListLayout() {
    super(PluginViewModel.class);
    setHeightFull();
  }

  public void setItems(List<PluginDto> plugins) {
    setViewModelItems(plugins.stream().map(PluginViewModel::fromPluginDto).collect(Collectors.toList()));
  }
}
