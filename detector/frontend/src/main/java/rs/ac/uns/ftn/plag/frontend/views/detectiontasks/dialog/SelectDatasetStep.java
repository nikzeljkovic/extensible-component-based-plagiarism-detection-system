package rs.ac.uns.ftn.plag.frontend.views.detectiontasks.dialog;

import java.util.stream.Collectors;

import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

import rs.ac.uns.ftn.plag.frontend.views.pluginscommon.AllItemsGrid;
import rs.ac.uns.ftn.plag.frontend.views.pluginscommon.PluginViewModel;
import rs.ac.uns.ftn.plag.restclient.clients.DatasetRestClient;
import rs.ac.uns.ftn.plag.restclient.clients.DetectorRestClient;
import rs.ac.uns.ftn.plag.restclient.dto.DetectionTaskDto;

public class SelectDatasetStep extends DialogStep<DetectionTaskDto> {

  private final DetectorRestClient detectorRestClient;

  private PluginViewModel pluginViewModel;

  public SelectDatasetStep(DatasetRestClient datasetRestClient, DetectorRestClient detectorRestClient) {
    this.detectorRestClient = detectorRestClient;
    addClassNames("detectiontasks-view", "flex", "flex-col", "h-full");

    VerticalLayout wrapperLayout = new VerticalLayout();
    wrapperLayout.setSizeFull();
    AllItemsGrid<PluginViewModel> grid = new AllItemsGrid<>(PluginViewModel.class);
    wrapperLayout.add(grid);
    add(wrapperLayout);

    grid
      .setItems(datasetRestClient.findAll().stream().map(PluginViewModel::fromPluginDto).collect(Collectors.toList()));

    grid.asSingleSelect().addValueChangeListener(event -> pluginViewModel = event.getValue());

    setHeight(600f, Unit.PIXELS);
    setWidthFull();
  }

  @Override
  public void fillResultDto(DetectionTaskDto resultDto) {
    if (pluginViewModel == null) {
      return;
    }
    resultDto.setDatasetName(pluginViewModel.getName());
  }

  @Override
  public String getStepDescription() {
    return "Select a reference document set";
  }

  @Override
  public DialogStep<DetectionTaskDto> getNextStep() {
    return new SelectDetectorStep(detectorRestClient);
  }

  @Override
  public boolean isValid() {
    return pluginViewModel != null;
  }

  @Override
  public String getInvalidMessage() {
    return "Please select a dataset";
  }
}
