package rs.ac.uns.ftn.plag.frontend.views.detectiontasks.dialog;

import com.vaadin.flow.component.html.Div;

public abstract class DialogStep<T> extends Div {

  public abstract void fillResultDto(T resultDto);

  public abstract String getStepDescription();

  public abstract DialogStep<T> getNextStep();

  public abstract boolean isValid();

  public abstract String getInvalidMessage();
}
