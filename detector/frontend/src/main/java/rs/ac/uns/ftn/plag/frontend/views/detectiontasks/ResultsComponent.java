package rs.ac.uns.ftn.plag.frontend.views.detectiontasks;

import com.vaadin.flow.component.html.Div;

import rs.ac.uns.ftn.plag.restclient.dto.DetectionResultDto;

public class ResultsComponent extends Div {

  public void update(DetectionResultDto resultDto) {
    removeAll();
    if (resultDto == null) {
      return;
    }
    if (resultDto.getMatches() == null) {
      return;
    }
    add(new ResultsDetailView(resultDto));
  }
}
