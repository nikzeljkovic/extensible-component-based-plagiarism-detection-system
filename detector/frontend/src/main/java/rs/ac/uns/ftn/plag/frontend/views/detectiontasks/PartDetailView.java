package rs.ac.uns.ftn.plag.frontend.views.detectiontasks;

import org.apache.commons.lang3.tuple.Pair;

import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public class PartDetailView extends VerticalLayout {

  public PartDetailView(Pair<String, String> part) {
    addClassName("box-shadow-type-1");
    add(new Label("Candidate: " + part.getLeft()));
    add(new Label("Reference: " + part.getRight()));
  }
}
