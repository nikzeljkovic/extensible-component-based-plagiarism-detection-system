package rs.ac.uns.ftn.plag.frontend.views.detectiontasks;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

public class MatchPartsDetailView extends HidableVerticalLayout {

  public MatchPartsDetailView(List<Pair<String, String>> similarParts) {
    super("Show similar parts", "Hide similar parts");
    if (similarParts == null || similarParts.isEmpty()) {
      setShowHideButtonVisible(false);
    } else {
      setShowHideButtonVisible(true);
    }

    similarParts.stream().map(PartDetailView::new).forEach(this::add);
  }
}
