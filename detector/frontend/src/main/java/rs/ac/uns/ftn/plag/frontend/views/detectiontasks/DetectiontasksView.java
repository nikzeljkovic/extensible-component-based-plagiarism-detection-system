package rs.ac.uns.ftn.plag.frontend.views.detectiontasks;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.Uses;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.splitlayout.SplitLayout;
import com.vaadin.flow.data.provider.Query;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

import rs.ac.uns.ftn.plag.frontend.config.LoggedInAllowed;
import rs.ac.uns.ftn.plag.frontend.views.MainLayout;
import rs.ac.uns.ftn.plag.frontend.views.detectiontasks.dialog.CreateDetectionTaskDialog;
import rs.ac.uns.ftn.plag.restclient.clients.DatasetRestClient;
import rs.ac.uns.ftn.plag.restclient.clients.DetectionTaskRestClient;
import rs.ac.uns.ftn.plag.restclient.clients.DetectorRestClient;
import rs.ac.uns.ftn.plag.restclient.clients.DocumentRestClient;
import rs.ac.uns.ftn.plag.restclient.dto.DetectionTaskDto;

@PageTitle("Detection tasks")
@Route(value = "tasks/:detectionTaskId?/:action?(edit)", layout = MainLayout.class)
@LoggedInAllowed
@Uses(Icon.class)
public class DetectiontasksView extends Div implements BeforeEnterObserver {

  private final String DETECTION_TASK_ID = "detectionTaskId";
  private final String DETECTION_TASK_EDIT_ROUTE_TEMPLATE = "tasks/%s/edit";

  private final Grid<DetectionTaskDto> grid = new Grid<>(DetectionTaskDto.class, true);

  private final DetectionTaskRestClient detectionTaskRestClient;

  private Label taskIdLabel;
  private ResultsComponent resultsComponent;

  public DetectiontasksView(DetectionTaskRestClient detectionTaskRestClient, DocumentRestClient documentRestClient,
      DatasetRestClient datasetRestClient, DetectorRestClient detectorRestClient) {
    this.detectionTaskRestClient = detectionTaskRestClient;

    addClassNames("detectiontasks-view", "flex", "flex-col", "h-full");

    // Create UI

    SplitLayout splitLayout = new SplitLayout();
    splitLayout.setSizeFull();

    createGridLayout(splitLayout);
    createDetailLayout(splitLayout);

    Button createTaskButton = new Button("Create task");
    createTaskButton
      .addClickListener(event -> new CreateDetectionTaskDialog(detectionTaskRestClient, documentRestClient,
          datasetRestClient, detectorRestClient, () -> loadGridItems(detectionTaskRestClient)).open());

    VerticalLayout layout = new VerticalLayout();
    layout.setSizeFull();
    layout.add(createTaskButton);
    layout.add(splitLayout);

    add(layout);

    // Configure Grid

    loadGridItems(detectionTaskRestClient);
    grid.addThemeVariants(GridVariant.LUMO_NO_BORDER);
    grid.setHeightFull();

    // when a row is selected or deselected, populate form
    grid.asSingleSelect().addValueChangeListener(event -> {
      if (event.getValue() != null) {
        UI.getCurrent().navigate(String.format(DETECTION_TASK_EDIT_ROUTE_TEMPLATE, event.getValue().getId()));
      } else {
        clearForm();
        UI.getCurrent().navigate(DetectiontasksView.class);
      }
    });
  }

  private void loadGridItems(DetectionTaskRestClient detectionTaskRestClient) {
    grid.setItems(page -> getPage(page, detectionTaskRestClient.findAll()).stream());
  }

  private List<DetectionTaskDto> getPage(Query<DetectionTaskDto, Void> query, List<DetectionTaskDto> all) {
    int startIndex = query.getPage() * query.getPageSize();
    if (startIndex >= all.size()) {
      return Collections.emptyList();
    }
    if (startIndex + query.getPageSize() >= all.size()) {
      return all.subList(startIndex, all.size());
    }
    return all.subList(startIndex, query.getPageSize());
  }

  @Override
  public void beforeEnter(BeforeEnterEvent event) {
    Optional<String> taskId = event.getRouteParameters().get(DETECTION_TASK_ID);
    if (taskId.isPresent()) {
      Optional<DetectionTaskDto> optionalDetectionTaskDto = detectionTaskRestClient.findById(taskId.get());
      if (optionalDetectionTaskDto.isPresent()) {
        populateForm(optionalDetectionTaskDto.get());
      } else {
        Notification.show(String.format("The requested detection task was not found, ID = %s", taskId.get()), 3000,
            Notification.Position.BOTTOM_START);
        // when a row is selected but the data is no longer available,
        // refresh grid
        refreshGrid();
        event.forwardTo(DetectiontasksView.class);
      }
    }
  }

  private void createDetailLayout(SplitLayout splitLayout) {
    Div detailWrapper = new Div();
    detailWrapper.setClassName("flex flex-col");
    detailWrapper.setWidth("400px");

    Div growingWrapper = new Div();
    growingWrapper.setClassName("p-l flex-grow");
    detailWrapper.add(growingWrapper);

    taskIdLabel = new Label("");
    resultsComponent = new ResultsComponent();

    Div detailLayout = new Div();
    detailLayout.add(taskIdLabel);
    detailLayout.add(resultsComponent);

    detailLayout.addClassName("full-width");
    growingWrapper.add(detailLayout);
    splitLayout.addToSecondary(detailWrapper);
  }

  private void createGridLayout(SplitLayout splitLayout) {
    Div wrapper = new Div();
    wrapper.setId("grid-wrapper");
    wrapper.setWidthFull();
    splitLayout.addToPrimary(wrapper);
    wrapper.add(grid);
  }

  private void refreshGrid() {
    grid.select(null);
    grid.getLazyDataView().refreshAll();
  }

  private void clearForm() {
    populateForm(null);
  }

  private void populateForm(DetectionTaskDto value) {
    if (value == null) {
      taskIdLabel.setText("");
      resultsComponent.removeAll();
    } else {
      taskIdLabel.setText("Task id: " + value.getId());
      resultsComponent.update(value.getDetectionResult());
    }
  }
}
