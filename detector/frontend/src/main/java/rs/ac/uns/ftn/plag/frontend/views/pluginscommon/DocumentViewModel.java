package rs.ac.uns.ftn.plag.frontend.views.pluginscommon;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DocumentViewModel {

  private String id;

  private String fileName;

  private Boolean processingDone;

  private String mimeType;

  private String md5;
}
