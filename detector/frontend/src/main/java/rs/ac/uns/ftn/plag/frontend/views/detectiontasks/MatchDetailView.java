package rs.ac.uns.ftn.plag.frontend.views.detectiontasks;

import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

import rs.ac.uns.ftn.plag.restclient.dto.MatchDto;

public class MatchDetailView extends VerticalLayout {

  public MatchDetailView(MatchDto match) {
    if (match == null) {
      return;
    }

    addClassName("box-shadow-type-1");

    String refName = match.getDocumentMetadata().getFileName();
    add(new Div(new Label("Reference document file name: " + refName)));
    add(new Div(new Label("Similarity score: " + match.getScore())));
    add(new MatchPartsDetailView(match.getSimilarParts()));
  }
}
