package rs.ac.uns.ftn.plag.frontend.views.detectiontasks.dialog;

import java.util.Optional;

import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

import rs.ac.uns.ftn.plag.restclient.clients.DatasetRestClient;
import rs.ac.uns.ftn.plag.restclient.clients.DetectionTaskRestClient;
import rs.ac.uns.ftn.plag.restclient.clients.DetectorRestClient;
import rs.ac.uns.ftn.plag.restclient.clients.DocumentRestClient;
import rs.ac.uns.ftn.plag.restclient.dto.DetectionTaskDto;

public class CreateDetectionTaskDialog extends Dialog {

  private DialogStep<DetectionTaskDto> createTaskStep;

  private DialogStep<DetectionTaskDto> nextStep;

  private final DetectionTaskDto detectionTaskDto = new DetectionTaskDto();

  public CreateDetectionTaskDialog(DetectionTaskRestClient detectionTaskRestClient,
      DocumentRestClient documentRestClient, DatasetRestClient datasetRestClient, DetectorRestClient detectorRestClient,
      Runnable successCallback) {
    VerticalLayout dialogLayout = new VerticalLayout();

    Label dialogTitle = new Label("Create detection task");
    Label stepDescription = new Label("");

    createTaskStep = new SelectDocumentStep(documentRestClient, datasetRestClient, detectorRestClient);
    nextStep = createTaskStep.getNextStep();

    stepDescription.setText(createTaskStep.getStepDescription());

    Button nextStepButton = new Button("Next step");
    nextStepButton.addClickListener(event -> {
      if (!createTaskStep.isValid()) {
        Notification.show(createTaskStep.getInvalidMessage());
        return;
      }
      createTaskStep.fillResultDto(detectionTaskDto);
      if (nextStep != null) {
        dialogLayout.replace(createTaskStep, nextStep);
        createTaskStep = nextStep;
        stepDescription.setText(createTaskStep.getStepDescription());
        nextStep = createTaskStep.getNextStep();
        return;
      }
      Optional<DetectionTaskDto> result = detectionTaskRestClient.create(detectionTaskDto);
      if (result.isPresent()) {
        successCallback.run();
        Notification.show("Created detection task.");
      } else {
        Notification.show("Could not create detection task.");
      }
      close();
    });

    dialogLayout.add(dialogTitle);
    dialogLayout.add(stepDescription);
    dialogLayout.add(createTaskStep);
    dialogLayout.add(nextStepButton);

    dialogLayout.setHorizontalComponentAlignment(FlexComponent.Alignment.CENTER, dialogTitle);

    add(dialogLayout);
    setWidth(900f, Unit.PIXELS);
  }
}
