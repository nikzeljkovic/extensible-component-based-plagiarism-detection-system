package rs.ac.uns.ftn.plag.frontend.views.pluginscommon;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import rs.ac.uns.ftn.plag.restclient.dto.PluginDto;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PluginViewModel {

  private String name;

  private String humanReadableName;

  private String description;

  private String requiredDocumentMetadata;

  private String dependencies;

  public static PluginViewModel fromPluginDto(PluginDto pluginDto) {
    String requiredDocumentMetadata = String.join(", ", pluginDto.getRequiredDocumentMetadata());
    String dependencies = String.join(", ", pluginDto.getDependencies());

    return new PluginViewModel(pluginDto.getName(), pluginDto.getHumanReadableName(), pluginDto.getDescription(),
        requiredDocumentMetadata, dependencies);
  }
}
