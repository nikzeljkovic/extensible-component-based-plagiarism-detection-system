package uns.ac.rs.ftn.pluginutils;

import static org.apache.commons.collections4.CollectionUtils.isEmpty;

import java.util.*;
import java.util.stream.Collectors;

import uns.ac.rs.ftn.coreinterfaces.port.common.Plugin;

public class PluginSorter {

  public <T extends Plugin> void sortByDependencies(List<T> processors) {
    Map<String, T> fieldNameToPlugin =
        processors.stream().collect(Collectors.toMap(p -> p.getPluginMetadata().getName(), p -> p));
    List<T> sorted = new ArrayList<>();
    Set<String> added = new HashSet<>();

    processors
      .forEach(p -> addAfterDependencies(p, sorted, added, fieldNameToPlugin, new ArrayList<>(), new HashSet<>()));
    processors.clear();
    processors.addAll(sorted);
  }

  private <T extends Plugin> void addAfterDependencies(T p, List<T> sorted, Set<String> added,
      Map<String, T> pluginNameToPlugin, List<T> dependencyPath, Set<T> visited) {
    visited.add(p);
    dependencyPath.add(p);
    if (isEmpty(p.getPluginMetadata().getDependencies())) {
      addIfNotAdded(p, sorted, added);
      return;
    }
    p.getPluginMetadata()
      .getDependencies()
      .stream()
      .filter(pluginName -> !added.contains(pluginName))
      .map(pluginName -> {
        if (!pluginNameToPlugin.containsKey(pluginName)) {
          throw new RuntimeException(
              "Dependency " + pluginName + " of " + p.getPluginMetadata().getName() + " not found.");
        }
        return pluginNameToPlugin.get(pluginName);
      })
      .map(p2 -> {
        verifyNoCyclicDependency(p, dependencyPath, visited, p2);
        return p2;
      })
      .forEach(d -> addAfterDependencies(d, sorted, added, pluginNameToPlugin, dependencyPath, visited));
    addIfNotAdded(p, sorted, added);
  }

  private <T extends Plugin> void verifyNoCyclicDependency(T p, List<T> dependencyPath, Set<T> visited, T dependency) {
    if (visited.contains(dependency)) {
      dependencyPath.add(dependency);

      List<Plugin> cyclePath = new ArrayList<>();
      boolean pathStart = false;
      for (Plugin processingPlugin : dependencyPath) {
        if (processingPlugin.equals(dependency)) {
          pathStart = true;
        }
        if (pathStart) {
          cyclePath.add(processingPlugin);
        }
      }

      throw new RuntimeException("Cyclic dependency detected:  "
          + cyclePath.stream().map(p2 -> p2.getPluginMetadata().getName()).collect(Collectors.joining(" -> ")));
    }
  }

  private <T extends Plugin> void addIfNotAdded(T p, List<T> sorted, Set<String> added) {
    if (added.contains(p.getPluginMetadata().getName())) {
      return;
    }
    sorted.add(p);
    added.add(p.getPluginMetadata().getName());
  }
}
