package uns.ac.rs.ftn.pluginutils;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import uns.ac.rs.ftn.coreinterfaces.port.common.Plugin;

public class PluginLoader {

  private static final PluginSorter pluginSorter = new PluginSorter();

  public <T extends Plugin> void load(String jarDirectoryPath, Class<T> parentClassOrInterface, List<T> instanceList) {
    List<Class<T>> classesToIgnore =
        instanceList.stream().map(Object::getClass).map(c -> (Class<T>) c).collect(Collectors.toList());

    try (Stream<Path> s = Files.walk(Paths.get(jarDirectoryPath))) {
      s.filter(Files::isRegularFile)
        .map(Path::toFile)
        .filter(f -> f.getName().endsWith(".jar"))
        .map(f -> this.createClassInstancesFromJarFile(f, parentClassOrInterface, classesToIgnore))
        .flatMap(Collection::stream)
        .forEach(instanceList::add);
    } catch (IOException e) {
      e.printStackTrace();
    }

    pluginSorter.sortByDependencies(instanceList);
  }

  private <T extends Plugin> List<T> createClassInstancesFromJarFile(File jarFile, Class<T> parentClassOrInterface,
      List<Class<T>> classesToIgnore) {
    try {
      Set<Class<T>> classes = getClassesFromJarFile(jarFile);

      Set<Class<T>> factoryClasses = classes.stream()
        .filter(c -> implementsInterface(c, parentClassOrInterface))
        .filter(c -> !classesToIgnore.contains(c))
        .collect(Collectors.toSet());

      return factoryClasses.stream()
        .map(this::tryInstantiateClass)
        .filter(Objects::nonNull)
        .collect(Collectors.toList());
    } catch (Exception e) {
      e.printStackTrace();
      return Collections.emptyList();
    }
  }

  private <T extends Plugin> T tryInstantiateClass(Class<T> c) {
    try {
      return (T) c.getConstructors()[0].newInstance();
    } catch (InstantiationException | IllegalAccessException | InvocationTargetException | ClassCastException e) {
      e.printStackTrace();
      return null;
    }
  }

  private <T> boolean implementsInterface(Class<?> c, Class<T> anInterface) {
    for (Class<?> anInterface2 : c.getInterfaces()) {
      if (anInterface2.equals(anInterface)) {
        return true;
      }
    }
    return false;
  }

  public static <T extends Plugin> Set<Class<T>> getClassesFromJarFile(File jarFile) throws IOException {
    Set<String> classNames = getClassNamesFromJarFile(jarFile);
    Set<Class<T>> classes = new HashSet<>(classNames.size());
    try (URLClassLoader cl = URLClassLoader.newInstance(new URL[] { new URL("jar:file:" + jarFile + "!/") })) {
      for (String name : classNames) {
        try {
          Class<?> aClass = cl.loadClass(name);
          classes.add((Class<T>) aClass);
        } catch (LinkageError | Exception ignore) {
        }
      }
    }
    return classes;
  }

  public static Set<String> getClassNamesFromJarFile(File givenFile) throws IOException {
    Set<String> classNames = new HashSet<>();
    try (JarFile jarFile = new JarFile(givenFile)) {
      Enumeration<JarEntry> e = jarFile.entries();
      while (e.hasMoreElements()) {
        JarEntry jarEntry = e.nextElement();
        if (jarEntry.getName().endsWith(".class")) {
          String className = jarEntry.getName().replace("/", ".").replace(".class", "");
          classNames.add(className);
        }
      }
      return classNames;
    }
  }
}
